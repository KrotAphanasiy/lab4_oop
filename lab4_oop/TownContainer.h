#pragma once
#include "Town.h"

class TownContainer {
public:
	TownContainer() = default;

	virtual void Insert(const Town& ref) = 0;
	virtual const Town* Search(double averageLSPrice, double greenZones) const = 0;
	virtual const Town* SearchOnCertain(double param, char paramId) const = 0;
	virtual ~TownContainer() = default;

};