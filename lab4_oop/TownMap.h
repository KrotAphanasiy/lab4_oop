#pragma once
#include "TownContainer.h"
#include "TownKey.h"
#include <map>

class TownMap : public TownContainer {
public:

	void Insert(const Town& ref) override;
	const Town* Search(double averageLSPrice, double greenZones) const override;
	const Town* SearchOnCertain(double param, char paramId) const override;

	~TownMap() override = default;
protected:
	std::map<TownKey, Town> _towns;
};

