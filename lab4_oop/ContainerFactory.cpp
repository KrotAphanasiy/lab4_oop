#include "ContainerFactory.h"

TownContainer* ContainerFactory::CreateContainer(char type, int elementsCount)
{
	if (type == 'V') {
		return new TownVector();
	}
	else if (type == 'M') {
		return new TownMap();
	}
	else {
		return nullptr;
	}
}