#pragma once
#include "TownContainer.h"
#include <vector>

class TownVector : public TownContainer {
public:

	void Insert(const Town& ref) override;
	const Town* Search(double averageLSPrice, double greenZones) const override;
	const Town* SearchOnCertain(double param, char paramId) const override;

	~TownVector() override = default;
protected:
	std::vector<Town> _towns;
};

