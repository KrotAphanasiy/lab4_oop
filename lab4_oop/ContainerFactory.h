#pragma once
#include "TownContainer.h"
#include "TownVector.h"
#include "TownMap.h"
 
class ContainerFactory {
public:
	TownContainer* CreateContainer(char type, int elementsCount = 0);
};

