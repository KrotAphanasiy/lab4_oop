#pragma once
#include <istream>
#include <sstream>

template<typename T>
T ParseValue(std::istream& stream) {
	std::string input;
	stream >> input;

	std::stringstream sinput(input);
	T toReturn;

	if (!(sinput >> toReturn)) {
		throw std::runtime_error("Wrong value input format");
	}

	return toReturn;
}
