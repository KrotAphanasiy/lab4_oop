#pragma once
#include "ContainerFactory.h"
#include <iostream>
#include <fstream>
#include <istream>
#include <ostream>

class ConsoleInteractor
{
public:
	ConsoleInteractor() = default;

	void Run();

	~ConsoleInteractor();
private:
	
	TownContainer* _container = nullptr;
	int _townsCount = 0;

	void ShowMenu();
	void ProcessCommand(int commandId);

	void ShowTownInfo(const Town* townPtr);
	void AddNewTown(std::istream& input);

};

