#include "TownMap.h"

void TownMap::Insert(const Town& ref) {
	TownKey key;
	key._greenZones = ref.GetGreenZones();
	key._averageLSPrice = ref.GetAverageLSPrice();
	_towns.insert(std::pair<TownKey, Town>(key, ref));
}

const Town* TownMap::Search(double averageLSPrice, double greenZones) const {
	TownKey key;
	key._averageLSPrice = averageLSPrice;
	key._greenZones = greenZones;

	auto it = _towns.find(key);
	if (it != _towns.end()) {
		return &it->second;
	}
	else {
		return nullptr;
	}
}

const Town* TownMap::SearchOnCertain(double param, char paramId) const {
	TownKey key;
	if (paramId == 'g') {
		key._greenZones = param;
	}
	else if (paramId == 'p') {
		key._averageLSPrice = param;
	}

	auto it = _towns.find(key);
	if (it != _towns.end()) {
		return &it->second;
	}
	else {
		return nullptr;
	}


}

bool operator<(const TownKey& ref1, const TownKey& ref2) {
	bool res = false;

	if (ref2._greenZones >= 0 && ref2._averageLSPrice < 0) {
		res = ref1._greenZones < ref2._greenZones;
	}
	else if (ref2._averageLSPrice >= 0 && ref2._greenZones < 0) {
		res = ref1._averageLSPrice < ref2._averageLSPrice;
	}
	else if (ref2._averageLSPrice >= 0 && ref2._greenZones >= 0) {
		res = ref1._greenZones < ref2._greenZones&& ref1._averageLSPrice < ref2._averageLSPrice;
	}

	return res;
}