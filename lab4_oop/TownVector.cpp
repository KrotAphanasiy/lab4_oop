#include "TownVector.h"

void TownVector::Insert(const Town& ref) {
	_towns.push_back(ref);
}

const Town* TownVector::Search(double averageLSPrice, double greenZones) const {
	auto it = std::find_if(_towns.begin(), _towns.end(), [averageLSPrice, greenZones](const Town& inVec) -> bool {
			return (inVec.GetAverageLSPrice() == averageLSPrice) && (inVec.GetGreenZones() == greenZones);
		});

	if (it != _towns.end()) {
		return &(*it);
	}
	else {
		return nullptr;
	}
}

const Town* TownVector::SearchOnCertain(double param, char paramId) const {
	auto it = std::find_if(_towns.begin(), _towns.end(), [param, paramId](const Town& inVec) -> bool {
		if (paramId == 'g') {
			return inVec.GetGreenZones() == param;
		}
		else if (paramId == 'p') {
			return inVec.GetAverageLSPrice() == param;
		}
		else {
			return false;
		}
		});

	if (it != _towns.end()) {
		return &(*it);
	}
	else {
		return nullptr;
	}
}