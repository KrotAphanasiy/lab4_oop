#include "ConsoleInteractor.h"
#include "ParseValue.h"
using std::cin;
using std::cout;
using std::cerr;

ConsoleInteractor::~ConsoleInteractor()
{
	delete _container;
}

void ConsoleInteractor::Run() {

	char containerType;
	cout << "Enter container type (V - vector, M - map): ";
	try {
		containerType = ParseValue<char>(cin);
	}
	catch (const std::exception& exc) {
		cerr << exc.what();
		return;
	}

	std::string filename;
	cout << "Enter towns storage file name: ";
	cin >> filename;

	std::fstream input(filename);
	if (!input.is_open()) {
		cerr << "File doesn`t exist!\n";
		return;
	}

	try {
		_townsCount = ParseValue<int>(input);
	}
	catch (const std::exception& exc) {
		cerr << exc.what();
		return;
	}

	ContainerFactory contCreator;
	_container = contCreator.CreateContainer(containerType, _townsCount);
	if (_container == nullptr) {
		cerr << "Wrong container format was entered earlier, now exiting...\n";
		return;
	}
		
	for (int counter = 0; counter < _townsCount; counter++) {
		AddNewTown(input);
	}

	ShowMenu();
	int action;

	while (true) {
		cout << ">> ";
		try {
			action = ParseValue<int>(cin);
		}catch(const std::exception& exc){
			cerr << exc.what();
			continue;
		}
		if (action != 0) {
			try {
				ProcessCommand(action);
			}
			catch (const std::exception& exc) {
				cerr << exc.what();
				continue;
			}
		}
		else {
			break;
		}
	}

}

void ConsoleInteractor::ShowMenu()
{
	cout << "\nSelect action:\n\n";
	cout << "1 - Search for town by green zones and average living space price\n";
	cout << "2 - Search for town by green zones only\n";
	cout << "3 - Search for town by living space price only\n";
	cout << "4 - Add new town to container\n";
	cout << "0 - exit program\n\n";
}

void ConsoleInteractor::ProcessCommand(int commandId)
{

	double averageLSPrice;
	double greenZones;
	cout << ">> ";

	if (commandId == 1) {
		cout << "Enter lsPrice and green zones\n>> ";
		averageLSPrice = ParseValue<double>(cin);
		greenZones = ParseValue<double>(cin);

		const Town* tempPtr = _container->Search(averageLSPrice, greenZones);
		ShowTownInfo(tempPtr);
	}
	else if (commandId == 2) {
		cout << "Enter green zones\n>> ";
		greenZones = ParseValue<double>(cin);

		const Town* tempPtr = _container->SearchOnCertain(greenZones, 'g');
		ShowTownInfo(tempPtr);
	}
	else if (commandId == 3) {
		cout << "Enter lsPrice\n>> ";
		averageLSPrice = ParseValue<double>(cin);

		const Town* tempPtr = _container->SearchOnCertain(averageLSPrice, 'p');
		ShowTownInfo(tempPtr);
	}
	else if (commandId == 4) {
		cout << "Enter town`s info like:\nName soul-income lsPrice green-zones\n>> ";
		AddNewTown(cin);
	}
	else {
		cout << "Wrong action number!\n";
	}
	
}

void ConsoleInteractor::ShowTownInfo(const Town* townPtr)
{
	if (townPtr == nullptr) {
		cout << "Town doesn`t exist, sry... o7\n";
		return;
	}

	cout << townPtr->GetName() << " --- " << "soul-income: " << townPtr->GetSoulIncome()
		<< " ls-price: " << townPtr->GetAverageLSPrice() << " green zones: " << townPtr->GetGreenZones() << std::endl;
}

void ConsoleInteractor::AddNewTown(std::istream& input)
{
	std::string name;
	input >> name;

	double soulIncome = ParseValue<double>(input);
	double lsPrice = ParseValue<double>(input);
	double greenZones = ParseValue<double>(input);

	Town town(name, soulIncome, lsPrice, greenZones);
	_container->Insert(town);
}