#include "Town.h"

Town::Town(std::string name, double averageSoulIncome, double averageLSPrice, double greenZones):
	_name(name),
	_averageSoulIncome(averageSoulIncome),
	_averageLSPrice(averageLSPrice),
	_greenZones(greenZones) {

}

Town::Town(const Town& town) : 
	_name(town._name),
	_averageLSPrice(town._averageLSPrice),
	_averageSoulIncome(town._averageSoulIncome),
	_greenZones(town._greenZones) {

}

const std::string& Town::GetName() const
{
	return _name;
}

double Town::GetSoulIncome() const
{
	return _averageSoulIncome;
}

double Town::GetAverageLSPrice() const
{
	return _averageLSPrice;
}

double Town::GetGreenZones() const
{
	return _greenZones;
}

Town& Town::operator=(const Town& town)
{
	if (this == &town) {
		return *this;
	}

	_name = town._name;
	_averageSoulIncome = town._averageSoulIncome;
	_averageLSPrice = town._averageLSPrice;
	_greenZones = town._greenZones;

	return *this;
}