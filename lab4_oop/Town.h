#pragma once
#include <string>

class Town {
public:
	Town() = default;
	Town(std::string name, double averageSoulIncome, double averageLSPrice, double greenZones);
	Town(const Town& town);

	Town& operator=(const Town& town);
	
	const std::string& GetName() const;
	double GetSoulIncome() const;
	double GetAverageLSPrice() const;
	double GetGreenZones() const;

	~Town() = default;

protected:
	std::string _name = "";
	double _averageSoulIncome = 0;
	double _averageLSPrice = 0;
	double _greenZones = 0;
};